﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test_10_10.Models.Entities
{
    public class Book
    {
        public string BookId { get; set; }
        public string BookName { get; set; }
    }
}
